' Revenue Manager Price Plan VB Script
' Floating Natural Gas Product
' Version 0.0.3
' John Edit 120pm
' Tested on Revenue Manager 8.0.0.101

CONST sScriptName = "NGFloat.vb"

CONST bDebug = True

CONST sUOM = "GJ"
CONST sCommodity = "NG"
CONST sPriceComponentName = "GAS_FLOATING"
CONST sGUPPriceName = "GUP"

CONST sReferenceDomain = "WEATHER ZONE"

' Start of Script 
oLog.WriteLine ""
oLog.WriteLine "Start - " & FormatDateTime(Now) & " - " & SCRIPT_NAME
oLog.WriteLine "========================================================"

' Initialize Manager objects
Set oABPMgr = CreateObject("ABPCom.ComABPManager")
oABPMgr.Initialize "", "", ""
Set oABPComUser = oABPMgr.Login(oUser.LoginId, oUser.Password)

Set oVendorMgr = CreateObject("ABPCom.ComVendorManager")
oVendorMgr.InitializeFromUser(oUser)

Set oCustMgr = CreateObject("ABPCom.ComCustomerManager")
oCustMgr.InitializeFromUser(oUser)

Set oTempZoneMgr = oABPComUser.GetTemperatureZoneManager()

Set oRefMgr = CreateObject("ComCore.ComRefMgr") 

' Initialize standard ABPCom objects
Set oBillingAccount = oInfo.GetBillingAccount()
Set oCustomer = oCustMgr.GetCustomer(oBillingAccount.CustomerId)
Set oLDCAccount = oInfo.GetAccount()
strLDCAccountID = oLDCAccount.ObjectId

' Get temperature zone
strTempZone = oLDCAccount.TemperatureZone
Set oTZones = oTempZoneMgr.GetTemperatureZones()

For Each oTZoneT in oTZones
    strTempZoneT = oTZoneT.TemperatureZoneID
    If (strTempZoneT = strTempZone) Then
        Set oTZone = oTZoneT
    End If
Next

strTempZoneName = OTZone.TemperatureZone

Set oRef = oRefMgr.GetReference(sReferenceDomain, strTempZoneName)
strPriceIndexName = oRef.GetAttribute("PRICE_PROFILE")

If bDebug = True Then oLog.WriteLine "Temperature Zone=" & strTempZoneName & ", Price Index=" & strPriceIndexName

lngProTemp = CLng(oRefMgr.GetSystemParameterValue("PROT"))

' Get usage totals and charge period dates
Call SubTotalUsageDUPeriod(valTotalQuantity, valTotalQuantityME, valTotalQuantityLL, valTotalQuantityUFE, strLDCAccountID, lngStartDate, lngEndDate)

' Validate temperature data
Call SubCalculateWAP(valWAPrice, oTZone, strPriceIndexName, lngProTemp, lngStartDate, lngEndDate)

' Get Price Component details -- $/kWh
blnPCFound = False
blnFPCFound = False
blnCPCFound = False
valServiceFee = GetPriceCompValue(blnPCFound, oCustMgr, sPriceComponentName, "SERVICE_FEE", "cLDCAccount", strLDCAccountID, lngStartDate, lngEndDate)
valPriceFloor = GetOptionalPriceCompValue(blnFPCFound, oCustMgr, sPriceComponentName, "PRICE_FLOOR", "cLDCAccount", strLDCAccountID, lngStartDate, lngEndDate)
valPriceCeiling = GetOptionalPriceCompValue(blnCPCFound, oCustMgr, sPriceComponentName, "PRICE_CEILING", "cLDCAccount", strLDCAccountID, lngStartDate, lngEndDate)

If (blnFPCFound = True And valWAPrice < valPriceFloor) Then
    oLog.WriteLine "WAP " & valWAPrice & " below floor " & valPriceFloor & ", using floor value."
    valWAPrice = valPriceFloor
End If

If (blnCPCFound = True And valWAPrice > valPriceCeiling) Then
    oLog.WriteLine "WAP " & valWAPrice & " above ceiling " & valPriceFloor & ", using ceiling value."
    valWAPrice = valPriceCeiling
End If

' If there is consumption, calculate energy charge
If (valTotalQuantity <> 0) Then
    valTotalCharge = valTotalQuantity * valWAPrice
    If bDebug = True Then oLog.WriteLine "Floating Natural Gas: valWAPrice=" & valWAPrice & ", valTotalQuantity=" & valTotalQuantity &", valTotalCharge=" & valTotalCharge
    Call CreateTransaction(valTotalCharge, valTotalQuantity, valWAPrice, "ESRV", "ENER", sUOM, "Floating Natural Gas", lngStartDate, lngEndDate)

    ' If there is a Service Fee, calcuatae charge
    IF (valServiceFee <> 0) Then
        valTotalCharge = valTotalQuantity * valServiceFee
        If bDebug = True Then oLog.WriteLine "Floating Natural Gas Service Fee: valServiceFee=" & valServiceFee & ", valTotalQuantity=" & valTotalQuantity &", valTotalCharge=" & valTotalCharge
        Call CreateTransaction(valTotalCharge, valTotalQuantity, valServiceFee, "ESRV", "SVCF", sUOM, "Service Fee", lngStartDate, lngEndDate)
    End If

Else
    If bDebug = True Then oLog.WriteLine "Skipped charge creation as usage is zero."
End If

oLog.WriteLine "End - " & FormatDateTime(Now) & " - "  & SCRIPT_NAME
oLog.WriteLine "========================================================"
oLog.WriteLine ""
' End of Script

' Calculate usage totals and charge period dates
Sub SubTotalUsageDUPeriod(ByRef valTotalQuantity, ByRef valTotalQuantityME, ByRef valTotalQuantityLL, ByRef valTotalQuantityUFE, strLDCAccountID, ByRef lngStartDate, ByRef lngEndDate)
    
    Set oUsages = oInfo.GetUsage(sUOM, "", "")
    
    ' Validate usage for applicable UOM was found
    If (oUsages.Count = 0) Then
        Err.Raise 1, sScriptName, "Unable to find usage with valid UOM (" & sUOM & ") for LDC Account " & strLDCAccountID & ", lngStartDate:" & L2D(lngStartDate) & ", lngEndDate: " & L2D(lngEndDate)
    End If

    valNumUsages = 0
    valTotalQuantityME = 0
    valTotalQuantityL = 0
    valTotalQuantityUFE = 0
    lngTempStartDate = 0
    lngTempEndDate = 0

    ' Loop through usage records to identify earliest start date, latest end date, and sum by qualifier
    oLog.WriteLine "Start Usage:"
    For Each oUsage in oUsages
        valNumUsages = valNumUsages + 1
        
        lngIBeginDate = oUsage.BeginDateAsNumber
        lngIEndDate = oUsage.EndDateAsNumber

        If ((lngTempStartDate = 0) OR (lngTempStartDate > lngIBeginDate)) Then
			lngTempStartDate = lngIBeginDate
		End If

		If ((lngTempEndDate = 0) OR (lngTempEndDate < lngIEndDate)) Then
            lngTempEndDate = lngIEndDate
		End If

        strQualifier = oUsage.QualifierCode

        If bDebug = True Then oLog.WriteLine valNumUsages & ". lngIStartDate=" & lngIBeginDate & ", lngIEndDate=" & lngIEndDate & ", strQualifier=" & strQualifier

        Select Case strQualifier
            Case "ST"
                ' Valid usage
                valQuantity = oUsage.QuantityDelivered
                valTotalQuantityME = valTotalQuantityME + valQuantity
            Case "ME"
                ' Valid usage
                valQuantity = oUsage.QuantityDelivered
                valTotalQuantityME = valTotalQuantityME + valQuantity
            Case "LL"
                If (sCommodity = "EL") Then
                    ' Handle Line Loss
                    valQuantity = oUsage.QuantityDelivered
                    valTotalQuantityLL = valTotalQuantityLL + valQuantity
                Else
                    ' Should not have Line Loss for non-electricity usage
                    Err.Raise 1, sScriptName, "Found usage with unexpected qualifier LL for Gas LDC Account " & strLDCAccountID & ", lngStartDate:" & L2D(lngStartDate) & ", lngEndDate: " & L2D(lngEndDate)
                End If
            Case "UE"
                If (sCommodity = "EL") Then
                    ' Handle UFE
                    valQuantity = oUsage.QuantityDelivered
                    valTotalQuantityUFE = valTotalQuantityUFE + valQuantity
                Else
                    ' Should not have UFE for non-electricity usage
                    Err.Raise 1, sScriptName, "Found usage with unexpected qualifier UE for Gas LDC Account " & strLDCAccountID & ", lngStartDate:" & L2D(lngStartDate) & ", lngEndDate: " & L2D(lngEndDate)
                End If
            Case Else
                oLog.WriteLine "Found and ignored usage with qualifier = " & strQualifier
        End Select

        valTotalQuantity = valTotalQuantityME + valTotalQuantityLL + valTotalQuantityUFE

    Next

    lngStartDate = lngTempStartDate
    lngEndDate = lngTempEndDate

End Sub

Sub SubCalculateWAP(ByRef valWAPrice, oTZone, strPriceIndexName, lngProTemp, lngStartDate, lngEndDate)
    
    daysTarget = ( Datediff("d", L2D(lngStartDate), L2D(lngEndDate)) + 1 )
    daysCount = 1
    valHDDTotal = 0.0
    valCostTotal = 0.0
    valTemperature = 18

    dDate = Dateadd("d", -1, L2D(lngStartDate))

    ' Loop through days in period
    Do While daysCount <= daysTarget

        dDate = Dateadd("d", 1, dDate)
        If bDebug = True Then oLog.WriteLine "dateAsNumber=" & dateAsNumber

        ' Get 5A Price
        valIndexValue = CSng(oInfo.GetIndexValue(sGUPPriceName, dDate))

        ' Get daily temperature
        
        valTemperature = CSng(oInfo.GetIndexValue(strPriceIndexName, dDate))

        ' Calculate daily HDD value
        If valTemperature >= lngProTemp Then
            ' All days 5A prices must be utilized in the price calculation
            valHDD = 0.01
        Else
            valHDD = Abs(lngProTemp - valTemperature)
        End If

        valHDDTotal = valHDDTotal + valHDD
        valCostTotal = valCostTotal + (valHDD * valIndexValue)

        daysCount = daysCount + 1
    Loop

    valWAPrice = valCostTotal / valHDDTotal

End Sub

' Standard helper functions and subroutines

' Subroutine to create transaction
' Works with RevenueManager 8.0.0.76 and above
' Created 01/10/2018 SMS
Sub CreateTransaction(nAmount, nQuantity, nPPU, sTypeCode, sSubTypeCd, sUOM, sComment, dStart, dEnd)

    If bDebug = True Then oLog.WriteLine ""
    If bDebug = True Then oLog.WriteLine "---------------------------------------------"
    If bDebug = True Then oLog.WriteLine "Starting CreateTransaction script."
    If bDebug = True Then oLog.WriteLine "---------------------------------------------"
    If bDebug = True Then oLog.WriteLine "|nAmount   |" & nAmount
    If bDebug = True Then oLog.WriteLine "|nQuantity |" & nQuantity
    If bDebug = True Then oLog.WriteLine "|nPPU      |" & nPPU
    If bDebug = True Then oLog.WriteLine "|sTypeCode |" & sTypeCode
    If bDebug = True Then oLog.WriteLine "|sSubTypeCd|" & sSubTypeCd
    If bDebug = True Then oLog.WriteLine "|sUOM      |" & sUOM
    If bDebug = True Then oLog.WriteLine "|sComment  |" & sComment
    If bDebug = True Then oLog.WriteLine "|dStart    |" & CStr(dStart)
    If bDebug = True Then oLog.WriteLine "|dEnd      |" & CStr(dEnd)
    If bDebug = True Then oLog.WriteLine "---------------------------------------------"

    If nQuantity = 0 then
      Set oT0 = oInfo.NewTransaction(CDbl(0))
      oT0.TransactionQuantity = CDbl(0)
      oT0.TransactionAmount = CDbl(0)
      oT0.PricePerUnitAmount = CCur(0)
      If Not sTypeCode = "" Then oT0.TransactionTypeCode = sTypeCode
      If Not sSubTypeCd = "" Then oT0.TransactionSubTypeCode = sSubTypeCd
      If Not sUOM = "" Then oT0.UnitTypeCode = sUOM
      If Not sComment = "" Then oT0.Comment = sComment
      oT0.Generic1Date = L2D(dStart)
      oT0.Generic2Date = L2D(dEnd)
	Else
      Set oT1 = oInfo.NewTransaction(nAmount)
      oT1.TransactionQuantity = Round(nQuantity, 1)
      oT1.PricePerUnitAmount = Round(nPPU, 10)
      If Not sTypeCode = "" Then oT1.TransactionTypeCode = sTypeCode
      If Not sSubTypeCd = "" Then oT1.TransactionSubTypeCode = sSubTypeCd
      If Not sUOM = "" Then oT1.UnitTypeCode = sUOM
      If Not sComment = "" Then oT1.Comment = sComment
      oT1.Generic1Date = L2D(dStart)
      oT1.Generic2Date = L2D(dEnd)
      If sTypeCode = "NPT" Then oT1.InvoiceInclusion = "E"
    End If

    If bDebug = True Then oLog.WriteLine "---------------------------------------------"
    If bDebug = True Then oLog.WriteLine "Ending CreateTransaction script."
    If bDebug = True Then oLog.WriteLine "---------------------------------------------"
    If bDebug = True Then oLog.WriteLine ""

End Sub 'CreateTransaction

Function GetPriceCompValue(ByRef bPCFound, oCustMgr, sPCDefName, sPCAttrName, sPCRelateClass, sPCRelateID, lStartDate, lEndDate)
	oLog.WriteLine "GetPriceCompValue(oCustMgr, sPCDefName, sPCRelateClass) sPCDefName=" & sPCDefName & _
	               ", sPCAttrName="    & sPCAttrName                                                & _
				   ", sPCRelateClass=" & sPCRelateClass                                             & _
				   ", lStartDate="     & lStartDate                                                 & _ 
				   ", lEndDate="       & lEndDate

    bPCFound = False

    Set PriceCompCrit = oCustMgr.GetRMPriceComponentCriteria()
    Select Case sPCRelateClass
        Case "cCustomer"
            PriceCompCrit.IncludeCustPC = 1
        Case "cAccount"
            PriceCompCrit.IncludeAcctPC = 1
        Case "cLDCAccount"
            PriceCompCrit.IncludeLdcPC = 1
        Case Else
            'Don't use criteria
    End Select
    PriceCompCrit.DefinitionName = sPCDefName
    PriceCompCrit.StartDate = L2D(lStartDate) 'CDate("2020-01-01")
    PriceCompCrit.EndDate = L2D(lEndDate) 'CDate("2020-01-01")
    PriceCompCrit.UseEffectiveDate = 1
    PriceCompCrit.RelateClassName = sPCRelateClass
    PriceCompCrit.RelateId = sPCRelateID
    
    pcAttributeValue = Empty

    numPrice = 0 
    Set PriceComponentList = oCustMgr.GetRMPriceComponents(PriceCompCrit)
  	If Not PriceComponentList.Count = 1 Then
        'Err.Raise 1, sScriptName, "Unable to find price component (" & sPCDefName & ": " & sPCAttrName & ") for account, lStartDate:" & lStartDate & ", lEndDate: " & lEndDate
        Err.Raise 1, sScriptName, "Unable to find price component (" & sPCDefName & ": " & sPCAttrName & ") for account " & PCRelateId & ", lStartDate:" & L2D(lStartDate) & ", lEndDate: " & L2D(lEndDate)
    End If
    
    Set pcAttributeValue = PriceComponentList.Item(1).GetValueByAttributeName(sPCAttrName)

    bPCFound = True

    GetPriceCompValue = pcAttributeValue.Value
    
End Function 'GetPriceCompValue

Function GetOptionalPriceCompValue(ByRef bPCFound, oCustMgr, sPCDefName, sPCAttrName, sPCRelateClass, sPCRelateID, lStartDate, lEndDate)
	oLog.WriteLine "GetOptionalPriceCompValue(oCustMgr, sPCDefName, sPCRelateClass) sPCDefName=" & sPCDefName & _
	               ", sPCAttrName="    & sPCAttrName                                                & _
				   ", sPCRelateClass=" & sPCRelateClass                                             & _
				   ", lStartDate="     & lStartDate                                                 & _ 
				   ", lEndDate="       & lEndDate

    bPCFound = False

    Set PriceCompCrit = oCustMgr.GetRMPriceComponentCriteria()
    Select Case sPCRelateClass
        Case "cCustomer"
            PriceCompCrit.IncludeCustPC = 1
        Case "cAccount"
            PriceCompCrit.IncludeAcctPC = 1
        Case "cLDCAccount"
            PriceCompCrit.IncludeLdcPC = 1
        Case Else
            'Don't use criteria
    End Select
    PriceCompCrit.DefinitionName = sPCDefName
    PriceCompCrit.StartDate = L2D(lStartDate) 'CDate("2020-01-01")
    PriceCompCrit.EndDate = L2D(lEndDate) 'CDate("2020-01-01")
    PriceCompCrit.UseEffectiveDate = 1
    PriceCompCrit.RelateClassName = sPCRelateClass
    PriceCompCrit.RelateId = sPCRelateID

    pcAttributeValue = Empty

    numPrice = 0 
    Set PriceComponentList = oCustMgr.GetRMPriceComponents(PriceCompCrit)
  	If Not PriceComponentList.Count = 1 Then
        GetOptionalPriceCompValue = 0
    Else
        Set pcAttributeValue = PriceComponentList.Item(1).GetValueByAttributeName(sPCAttrName)
        If (pcAttributeValue.Value = "") Then 
            GetOptionalPriceCompValue = 0
        Else
            bPCFound = True
            GetOptionalPriceCompValue = pcAttributeValue.Value
        End If
    End If
    
End Function 'GetOptionalPriceCompValue

'number yyyymmdd -> date
Function L2D(l) 
 s = CStr(l)

 s = Left(s, 4) & "-" & Mid(s, 5, 2)  & "-" & Mid(s, 7)

 L2D = CDate(s)
End Function

' mm/dd/yyyy to number yyyymmdd
Function D2N(s) 
	m = Left(s, 2)
	d = Mid(s, 4, 2)
	y = Mid(s, 7)

	res = y & m & d

    D2N = CLng(res)
End Function